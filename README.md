Notebooks
-------

* [Row Chart](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/dc.js-in-ipython-notebooks/raw/master/notebooks/DC.js%20-%20Row%20Chart.ipynb)
* [Pie Chart](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/dc.js-in-ipython-notebooks/raw/master/notebooks/DC.js%20-%20Pie%20Chart.ipynb)
* [Line Chart](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/dc.js-in-ipython-notebooks/raw/master/notebooks/DC.js%20-%20Line%20Chart.ipynb)
* [Bubble Chart](http://nbviewer.ipython.org/urls/bitbucket.org/hrojas/dc.js-in-ipython-notebooks/raw/master/notebooks/DC.js%20-%20Bubble%20Chart.ipynb)
* [Data Table](https://gumroad.com/hedaro)
* [Dashboard #1](https://gumroad.com/hedaro) 
* [Dashboard #2](https://gumroad.com/hedaro) 
* [Dashboard #3](https://gumroad.com/hedaro) 
* [Dashboard #4](https://gumroad.com/hedaro) 
* [more tutorials...](https://gumroad.com/hedaro)